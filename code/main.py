import pickle
from encoder import *
from dataset import *
from function import *
from network import *
from train import *
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader

#--------------------------------#
#            Load Data           #
#--------------------------------#
triggle = input('Are you save the dataset(y/n):')
if triggle == 'n':
    print('extract data...')
    data_list, label_list = generate_data(path='/home/liruixin/workspace/imu_classification/data')
    combined = list(zip(data_list, label_list))
    random.shuffle(combined)
    data_list, label_list = zip(*combined)
    X_train, X_test, Y_train, Y_test = train_test_split(data_list, label_list, test_size=0.2, random_state=42)
    train_dataset = IMU_dataset(X_train, Y_train)
    test_dataset = IMU_dataset(X_test, Y_test)
    with open(f'/home/liruixin/workspace/imu_classification/dataset_file/train_dataset_{time_step}.pkl', 'wb') as file:
        pickle.dump(train_dataset, file)
    with open(f'/home/liruixin/workspace/imu_classification/dataset_file/test_dataset_{time_step}.pkl', 'wb') as file:
        pickle.dump(test_dataset, file)
if triggle == 'y':
    print('load data...')
    with open(f'/home/liruixin/workspace/imu_classification/dataset_file/train_dataset_{time_step}.pkl', 'rb') as file:
        train_dataset = pickle.load(file)
    with open(f'/home/liruixin/workspace/imu_classification/dataset_file/test_dataset_{time_step}.pkl', 'rb') as file:
        test_dataset = pickle.load(file) 
train_dataloader  = DataLoader(train_dataset,batch_size=batch_size)
test_dataloader = DataLoader(test_dataset,batch_size=len(test_dataset)//10)
#--------------------------------#
#           Load Network         #
#--------------------------------#

# synnet_spike = synnet_spike
# synnet_vmem = synnet_vmem
synnet_fspike = synnet_fspike 
ann = ann

#——————————————————————————#
#          Training        #
#——————————————————————————#

print('training begin')

# snn_train_spike(device=device,
#                train_dataloader=dataloader,
#                model=synnet_spike)


# snn_train_vmem(device=device,
#                train_dataloader=dataloader,
#                model=synnet_vmem)


snn_train_first_spike(device=device,
               train_dataloader=train_dataloader,
               test_dataloader = test_dataloader,
               model=synnet_fspike)


# ann_train(device=device,
#           train_dataloader=train_dataloader, 
#           test_dataloader = test_dataloader,
#           ann=ann
#           )