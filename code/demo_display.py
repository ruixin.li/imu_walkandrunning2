import tkinter as tk
from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import torch
import pandas as pd
from tkinter import filedialog
from network import *
import torch.nn as nn
# - Matplotlib
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [12, 4]
plt.rcParams['figure.dpi'] = 300
plt.style.use('seaborn')
# - Rockpool time-series handling
from rockpool import TSEvent, TSContinuous
import torch

# - Disable warnings
import pickle
import warnings
warnings.filterwarnings('ignore')
# - Import the computational modules and combinators required for the networl
from rockpool.nn.modules import LIFTorch, LinearTorch
from rockpool.nn.combinators import Sequential, Residual
from rockpool.transform import quantize_methods as q
from torch.utils.data import Dataset,random_split,DataLoader
from pathlib import Path
from rockpool.devices import xylo as x
from rockpool.devices.xylo import imu 
from sklearn.metrics import accuracy_score, f1_score, precision_recall_fscore_support, confusion_matrix
from imu_preprocessing.chip_IMU_filters import ChipButterworth
from imu_preprocessing.spike_encoder import ScaleSpikeEncoder
from parameter import *
from function import *
from dataset import *

#==============================================================#
#           define the network and Xyloimu-smi                 #
#==============================================================#
time_step = time_step
num_channel = num_channel
num_class = num_class
thr_out = thr_out
synnet = synnet_fspike
synnet.load('/home/ruixing/workspace/imu_classification/models/model_fspike_600_0.9736441966548404.pth')

g = synnet.as_graph()
spec = imu.mapper(g, weight_dtype='float', threshold_dtype='float', dash_dtype='float')
quant_spec = spec.copy()
# - Quantize the specification
spec.update(q.global_quantize(**spec,fuzzy_scaling=True))
# spec['threshold_out'] = np.array([thr,thr])
# spec['bias_out'] = np.array([0,300])
# - Use rockpool.devices.xylo.config_from_specification
config, is_valid, msg = imu.config_from_specification(**spec)
modSim = imu.XyloSim.from_config(config)


t = 1
spike_list = []
# fig1 = plt.figure(figsize=(1.5, 1))
# fig2 = plt.figure(figsize=(1.5, 1.5))
# fig3 = plt.figure(figsize=(1.5, 1))
path = '/home/ruixing/workspace/imu_classification/data'
imu_encoder = create_imu_encoder()
def open_file():
    global path
    global target
    file_path = filedialog.askopenfilename(filetypes=[("Excel Files", "*.csv")])
    # if file_path:
        # with open(file_path, "r",encoding='ISO-8859-1') as file:
        #     content = file.read()
        #     text_box.delete("1.0", tk.END)  # 清空文本框
        #     text_box.insert(tk.END, content)

        # # 更新root变量为选中的文件路径
        # path = file_path
        # dic = ['jogging','walking']
        # target = 0 if Path(path).parts[-4][0] == 'j' else 1
        # success_label.config(text="加载数据成功")
        # success_label.grid(row=0, column=0, padx=150, pady=10, sticky="nw")
        
    # 更新root变量为选中的文件路径
    path = file_path
    dic = ['jogging','walking']
    target = 0 if Path(path).parts[-4][0] == 'j' else 1
    success_label.config(text="加载数据成功")
    # success_label.grid(row=0, column=0, padx=150, pady=10, sticky="nw")
        
def plot_original_signal():
    global path
    global array
    # global fig1
    global t
    # plt.figure(fig1.number)
    plt.clf()
    raw = pd.read_csv(path)
    data = (raw.iloc[:,1:4]).values
    array = data[0:t,:].reshape(-1,3)
    num_channels = data.shape[1]
    
    fig, axes = plt.subplots(num_channels, 1, figsize=(3.5, 1), sharex=True)
    colors = ['red','green', 'blue']
    for channel_idx in range(num_channels):
        if t >= 25:
            axes[channel_idx].plot(array[t-25:t,channel_idx], color=colors[channel_idx], linewidth=0.8,alpha=0.5)  
        else:
            axes[channel_idx].plot(array[:,channel_idx], color=colors[channel_idx], linewidth=0.8,alpha=0.5)  
        axes[channel_idx].set_xticks([])  # 去除 X 轴刻度和标签
        # axes[channel_idx].set_yticks([])  # 去除 Y 轴刻度和标签
        # axes[channel_idx].axis('off')  # 去除子图的边框和背景
        axes[channel_idx].tick_params(axis='both', which='both', labelsize=5)
        axes[channel_idx].scatter(25, np.mean(array[ :,channel_idx]), s=150, color=colors[channel_idx], marker='.')
    plt.suptitle("Signal", fontsize=12, y=1.0)
    fig = plt.gcf()
    # fig.set_size_inches(5, 2)
    canvas = FigureCanvasTkAgg(fig, master=root)
    canvas.draw()
    # 放置图形方框在中间偏上的位置
    canvas.get_tk_widget().grid(row=1, column=0, padx=10, pady=10)
    plot_spiking()
    
def plot_spiking():  
    global t
    # global fig2
    global array
    
    i = np.vstack((np.tile(array[0:1,:],(1000,1)),array))
    spike = generate_spikes(i.T,imu_encoder)
    spike = (spike.T)[-1,:]
    spike_list.append(spike)
    spike_array= np.array(spike_list)
    fig2, axes = plt.subplots(num_channel, 1, figsize=(3.5, 3), sharex=True, sharey=True)
    colors = ['red','red','red','red','red', 'green','green','green','green','green','blue','blue','blue','blue','blue']
    for channel_idx in range(num_channel):
        if t >= 25:
            axes[channel_idx].plot(spike_array[t-25:t,channel_idx], color=colors[channel_idx], linewidth=0.8,alpha=0.5)  
        else:
            axes[channel_idx].plot(spike_array[:,channel_idx], color=colors[channel_idx], linewidth=0.8,alpha=0.5)  
        axes[channel_idx].set_xticks([])  # 去除 X 轴刻度和标签
        axes[channel_idx].set_ylim(0, 14)
        # axes[channel_idx].set_yticks([])  # 去除 Y 轴刻度和标签
        # axes[channel_idx].axis('off')  # 去除子图的边框和背景
        axes[channel_idx].tick_params(axis='both', which='both', labelsize=5)
        axes[channel_idx].scatter(25, np.mean(spike_array[ :,channel_idx]), s=150, color=colors[channel_idx], marker='.')
    plt.suptitle("Spikes", fontsize=12, y=1.0)
    plt.subplots_adjust(hspace=0)
    fig2 = plt.gcf()
    # fig2.set_size_inches(4, 4)
    canvas = FigureCanvasTkAgg(fig2, master=root)
    canvas.draw()
    # 放置图形方框在中间偏上的位置
    canvas.get_tk_widget().grid(row=2, column=0, padx=10, pady=10)
    t = t+1
        
root = tk.Tk()
root.title("Tkinter Demo")
root.geometry("2400x1200")
root.configure(bg="white")
button_select_file = tk.Button(root, text="select csv file", command=open_file)
button_select_file.grid(row=0, column=0, padx=10, pady=10, sticky="nw")
text_box = tk.Text(root, wrap=tk.WORD)
text_box.grid_forget()
button_plot_graph = tk.Button(root, text="begin to detect", command=plot_original_signal)
button_plot_graph.grid(row=0, column=1, padx=10, pady=10, sticky="ne")
success_label = tk.Label(root, text="", fg="green")
success_label.grid(row=0, column=0, padx=150, pady=10, sticky="nw")

root.mainloop()

