# - Numpy
import numpy as np
import torch
from network import *
import torch.nn as nn
# - Matplotlib
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [12, 4]
plt.rcParams['figure.dpi'] = 300
plt.style.use('seaborn')
# - Rockpool time-series handling
from rockpool import TSEvent, TSContinuous
import torch

# - Disable warnings
import pickle
import warnings
warnings.filterwarnings('ignore')
# - Import the computational modules and combinators required for the networl
from rockpool.nn.modules import LIFTorch, LinearTorch
from rockpool.nn.combinators import Sequential, Residual
from rockpool.transform import quantize_methods as q
from torch.utils.data import Dataset,random_split,DataLoader
from pathlib import Path
from tqdm.asyncio import tqdm
from rockpool.devices import xylo as x
from rockpool.devices.xylo import imu 
from rockpool.nn.networks import SynNet,WaveSenseNet    
from sklearn.metrics import accuracy_score, f1_score, precision_recall_fscore_support, confusion_matrix
from rockpool.nn.combinators import Sequential, Residual
from rockpool.nn.modules.torch.lif_torch import PeriodicExponential
from rockpool.nn.modules import LIFBitshiftTorch
from imu_preprocessing.quantizer import Quantizer
from imu_preprocessing.chip_IMU_filters import ChipButterworth
from imu_preprocessing.spike_encoder import ScaleSpikeEncoder
from parameter import *
from function import *
from dataset import *


batch_size = 256
time_step = time_step
num_channel = num_channel
num_class = num_class
thr_out = thr_out
synnet = synnet_fspike
synnet.load('/home/liruixin/workspace/imu_classification/models/model_fspike_1000_0.980544747081712.pth')

g = synnet.as_graph()
spec = imu.mapper(g, weight_dtype='float', threshold_dtype='float', dash_dtype='float')
quant_spec = spec.copy()
# - Quantize the specification
spec.update(q.global_quantize(**spec))
thr = 5000
# spec['threshold_out'] = np.array([thr,thr])
# spec['bias_out'] = np.array([0,300])
# - Use rockpool.devices.xylo.config_from_specification
config, is_valid, msg = imu.config_from_specification(**spec)
modSim = imu.XyloSim.from_config(config)
