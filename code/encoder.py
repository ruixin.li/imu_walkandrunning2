from imu_preprocessing.subspace_tracking import SubSpace
from imu_preprocessing.sample_and_hold import SampleAndHold
from imu_preprocessing.JSVD2 import JSVD2, RotationLookUpTable2
from imu_preprocessing.rotation_removal import RotationRemoval
from imu_preprocessing.chip_IMU_filters import ChipButterworth
from imu_preprocessing.spike_encoder import IAFSpikeEncoder, ScaleSpikeEncoder
from imu_preprocessing.imu_chain import IMU
from imu_preprocessing.quantizer import Quantizer
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn')

# Define the number of bits for input signals
num_bits_in = 12

def create_imu_encoder(        
        num_angles=64,                 # Number of angles for encoding rotation angles, default is 64.
        num_bits=16,                   # Number of bits in input signals, default is 16.
        num_bits_in=num_bits_in,       # Number of bits in input signals, default is the previously defined num_bits.
        num_avg_bitshift=6,            # Bitshift for averaging, used for average calculations, default is 6.
        sampling_period=128,           # Sampling period for encoding, default is 128.
        num_bits_covariance=31,        # Number of bits for covariance, default is 31.
        num_bits_rotation=32,          # Number of bits for rotation, default is 32.
        nround=4,                      # Number of rounds for encoding iterations, default is 4.
        num_bits_out=32,               # Number of bits in output signals, default is 32.
        num_scale_bits=9,              # Number of bits for scaling, used for scaling, default is 8.
        num_spk_bits_out=4,            # Number of bits in output spike encoding, default is 4.
        iaf_threshold=6500,            # IAF threshold for spike encoding, default is 2000.
        spike_encode_mode="scale"      # Spike encoding mode, default is "scale".
        ):

    # Number of bits for multiplication result
    num_bits_multiplier = 2 * num_bits_in - 1 

    # Lookup table for rotation angle calculations
    lut = RotationLookUpTable2(num_angles=num_angles, num_bits=num_bits)

    # Number of bits for high-precision filter
    num_bits_highprec_filter = num_bits_multiplier + num_avg_bitshift 

    # Create Subspace matrix
    subspace = SubSpace(
        num_bits_in=num_bits_in,
        num_bits_multiplier=num_bits_multiplier,
        num_bits_highprec_filter=num_bits_highprec_filter,
        num_avg_bitshift=num_avg_bitshift,
    )

    # Create Sample and Hold sampler
    sampler = SampleAndHold(
        sampling_period=sampling_period,
    )

    # Create JSVD2 model
    jsvd = JSVD2(
        lookuptable=lut,
        num_bits_covariance=num_bits_covariance,
        num_bits_rotation=num_bits_rotation,
        nround=nround,
    )

    # Create Rotation Removal model
    rot_removal = RotationRemoval(
        subspace=subspace,
        sampler=sampler,
        jsvd=jsvd,
        num_bits_out=num_bits_out,
    )

    # Create Filterbank model
    filterbank = ChipButterworth()

    # Create Encoder
    if spike_encode_mode == "iaf":
        spk_enc = IAFSpikeEncoder(iaf_threshold=iaf_threshold)

    elif spike_encode_mode == "scale":
        spk_enc = ScaleSpikeEncoder(
            num_scale_bits=num_scale_bits,
            num_out_bits=num_spk_bits_out,
        )
        
    # Create IMU
    imu = IMU(
        rotation_removal=rot_removal,
        filterbank=filterbank,
        spk_enc=spk_enc,
    )

    return imu

def generate_spikes(signal, imu, num_bits_in=num_bits_in):
    Q = Quantizer()
    sig_in = Q.quantize(
        sig_in=signal,
        scale=(0.25),
        num_bits=num_bits_in,
        )
    rmspikes, spikes = imu.evolve(sig_in=sig_in)    
    spikes = np.array(spikes, dtype=int)
    spikes = np.vstack((spikes[i : i + 5, :] for i in [0, 16, 32]))

    return spikes
