import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from function import *
from tqdm.auto import tqdm
from sklearn.metrics import accuracy_score, f1_score, precision_recall_fscore_support, confusion_matrix
from sklearn.metrics import f1_score, precision_recall_fscore_support, confusion_matrix
from sklearn.metrics import f1_score, precision_recall_fscore_support, confusion_matrix
from Loss_fun import *
from parameter import *

def ann_train(device,train_dataloader,test_dataloader,ann):
    criterion = nn.CrossEntropyLoss()
    criterion.to(device)
    ann.to(device)
    # opt = optim.Adam(ann.parameters(), lr=0.000172)
    opt = optim.Adam(ann.parameters(), lr=0.000172, weight_decay=0.001)
    
    for epoch in range(num_epochs):
        ann.train()
        total_loss = 0.0
        correct_predictions = 0
        
        for inputs, labels in tqdm(train_dataloader):
            inputs = (inputs.reshape(-1,1,time_step,num_channel)).to(device)
            # inputs[inputs == -1] = 2
            labels = labels.to(device)
            opt.zero_grad()
            outputs = (ann(inputs)).to(device)
            # outputs = torch.sum(out,dim=1)
            # labels = labels.to(torch.long)
            loss = criterion(outputs, labels)
            _, predicted = torch.max(outputs, 1)


            loss.backward()
            opt.step()
        
        # 计算测试集的损失和准确率
        test_loss = 0.0
        test_correct_predictions = 0
        
        ann.eval()
        with torch.no_grad():
            for inputs, labels in test_dataloader:
                inputs = inputs.reshape(-1,1,time_step,num_channel).to(device)
                # inputs[inputs == -1] = 2
                labels = labels.to(device)
                outputs = ann(inputs).to(device)
                # outputs = torch.sum(outputs,dim=1)
                labels = labels.to(torch.long)
                loss = criterion(outputs, labels).to(device)
                _, predicted = torch.max(outputs, 1)
                # _, label = torch.max(labels, 1)
                
                test_loss += loss.item()
                test_correct_predictions += torch.sum(predicted == labels).item()
        
        # 输出训练集和测试集的损失和准确率
        train_loss = total_loss / len(train_dataloader)
        train_accuracy = correct_predictions / len(train_dataloader.dataset)
        test_loss /= len(test_dataloader)
        test_accuracy = test_correct_predictions / len(test_dataloader.dataset)
        print("Epoch", epoch+1)
        print("Train Set:")
        print("Loss:", train_loss)
        print("Accuracy:", train_accuracy)
        print("Test Set:")
        print("Loss:", test_loss)
        print_colorful_text(f"Accuracy:{test_accuracy}", 'yellow')
        ann.eval()
        true_labels = []
        predicted_labels = []
        with torch.no_grad():
            for inputs, labels in test_dataloader:
                inputs = inputs.reshape(-1,1,time_step,num_channel).to(device)
                labels.to(device)
                outputs = ann(inputs).to(device)
                # outputs = torch.sum(outputs,dim=1)
                _, predicted = torch.max(outputs, 1)
                # _, label = torch.max(labels, 1)
                true_labels.extend(labels.tolist())
                predicted_labels.extend(predicted.tolist())
        cm = confusion_matrix(true_labels, predicted_labels)
        print("Confusion Matrix:")
        print(cm)
        
        


def snn_train_spike(device, train_dataloader, test_dataloader, model):
    criterion = nn.CrossEntropyLoss()
    criterion.to(device)
    model.to(device)
    print('device:',device)
    opt = optim.Adam(model.parameters().astorch(), lr=0.000172)
    # scheduler = lr_scheduler.StepLR(opt, step_size=20, gamma=0.1)
    losslist = []
    accuracy = []
    f1s = []
    precision = []
    recall = []
    cmlist= []
    for epoch in range(2000):
        # scheduler.step()
        train_preds = []
        train_targets = []
        sum_loss = 0.0
        for batch, target in tqdm(train_dataloader):
            batch = batch.to(torch.float32).to(device)
            # target_loss = (torch.tensor(encode_labels(target,Nout,thr_out+0.2))).float().to(device)
            target_loss = target.to(device)
            model.reset_state()
            opt.zero_grad()
            out_model, _, rec = model(batch, record=True)
            out = torch.sum(out_model,dim=1)
            loss = criterion(out, target_loss)
            loss.backward()
            opt.step()

            with torch.no_grad():
                pred = out.argmax(1).detach().to(device)
                train_preds += pred.detach().cpu().numpy().tolist()
                train_targets += target.detach().cpu().numpy().tolist()
                sum_loss += loss.item() / len(train_dataloader)

        sum_f1 = f1_score(train_targets, train_preds, average="macro")
        _, train_precision, train_recall, _ = precision_recall_fscore_support(
            train_targets, train_preds, labels=np.arange(2)
        )
        train_accuracy = accuracy_score(train_targets, train_preds)

        print(f"Train Epoch = {epoch+1}, Loss = {sum_loss}, F1 Score = {sum_f1}")
        print(f"Train Precision = {train_precision}, Recall = {train_recall}")
        print(f"Train Accuracy = {train_accuracy}")

        test_preds = []
        test_targets = []
        test_loss = 0.0
        for batch, target in tqdm(test_dataloader):
            with torch.no_grad():
                batch = batch.to(torch.float32).to(device)
                model.reset_state()
                out_model, _, rec = model(batch, record=True)
                out = torch.sum(out_model,dim=1)
                pred = out.argmax(1).detach().to(device)
                test_preds += pred.detach().cpu().numpy().tolist()
                test_targets += target.detach().cpu().numpy().tolist()
 
        f1 = f1_score(test_targets, test_preds, average="macro")
        _, test_precision, test_recall, _ = precision_recall_fscore_support(
            test_targets, test_preds, labels=np.arange(2)
        )
        test_accuracy = accuracy_score(test_targets, test_preds)
        cm = confusion_matrix(test_targets, test_preds)
        # losslist.append(test_loss)
        f1s.append(f1)
        precision.append(test_precision)
        recall.append(test_recall)
        accuracy.append(test_accuracy)
        cmlist.append(cm)
        print(f"F1 Score = {f1}")
        print(f"Val Precision = {test_precision}, Recall = {test_recall}")
        print_colorful_text(f"Accuracy:{test_accuracy}", 'yellow')
        print("Confusion Matrix:")
        print(cm)
        if (epoch %100 == 0 and epoch != 0) or np.all(test_precision > 0.85):
            model.save(f'models/model_first_spike_{epoch}_{test_accuracy}.pth')
            print('模型已保存')
            np.save('train_data_record/loss.npy', losslist)
            np.save('train_data_record/f1s.npy', f1s)
            np.save('train_data_record/precision.npy', precision)
            np.save('train_data_record/recall.npy', recall)
            np.save('train_data_record/accuracy.npy', accuracy)
            np.save('train_data_record/cm.npy', cmlist)
            print('训练已完成，训练参数已保存') 
            
def snn_train_vmem(device, train_dataloader, test_dataloader, model):
    criterion = nn.CrossEntropyLoss()
    criterion.to(device)
    model.to(device)
    print('device:',device)
    opt = optim.Adam(model.parameters().astorch(), lr=0.0000172)
    # scheduler = lr_scheduler.StepLR(opt, step_size=20, gamma=0.1)
    losslist = []
    accuracy = []
    f1s = []
    precision = []
    recall = []
    cmlist= []
    for epoch in range(2000):
        # scheduler.step()
        train_preds = []
        train_targets = []
        sum_loss = 0.0
        for batch, target in tqdm(train_dataloader):
            batch = batch.to(torch.float32).to(device)
            # target_loss = (torch.tensor(encode_labels(target,Nout,thr_out))).float().to(device)
            target = target.to(device)
            model.reset_state()
            opt.zero_grad()
            out_model, _, rec = model(batch, record=True)
            out = torch.sum(rec['4_LIFExodus']['vmem'],dim=1).to(device)
            # peaks = torch.sum(out, dim=1).to(device)
            loss = criterion((out), target)
            loss.backward()
            opt.step()

            with torch.no_grad():
                pred = out.argmax(1).detach().to(device)
                train_preds += pred.detach().cpu().numpy().tolist()
                train_targets += target.detach().cpu().numpy().tolist()
                sum_loss += loss.item() / len(train_dataloader)

        sum_f1 = f1_score(train_targets, train_preds, average="macro")
        _, train_precision, train_recall, _ = precision_recall_fscore_support(
            train_targets, train_preds, labels=np.arange(2)
        )
        train_accuracy = accuracy_score(train_targets, train_preds)

        print(f"Train Epoch = {epoch+1}, Loss = {sum_loss}, F1 Score = {sum_f1}")
        print(f"Train Precision = {train_precision}, Recall = {train_recall}")
        print(f"Train Accuracy = {train_accuracy}")

        test_preds = []
        test_targets = []
        test_loss = 0.0

        for batch, target in tqdm(test_dataloader):
            with torch.no_grad():
                batch = batch.to(torch.float32).to(device)
                target = target.to(device)
                # target_loss = (torch.tensor(encode_labels(target,Nout,thr_out))).float().to(device)
                model.reset_state()
                out_model, _, rec = model(batch, record=True)
                print('脉冲数:',out_model.sum())
                # peaks = torch.sum(rec['spk_out']['vmem'], dim=1).to(device)
                # peaks = torch.sum(out, dim=1).to(device)
                # pred = peaks.argmax(1).detach().to(device)
                out = torch.sum(rec['4_LIFExodus']['vmem'],dim=1)
                # loss = criterion((out), target_loss)
                pred = out.argmax(1).detach().to(device)
                # data = (rec['4_LIFExodus']['vmem']).cpu().detach().numpy().reshape(3000,2)
                # fig, ax = plt.subplots()
                # for i in range(2):
                #     ax.plot(data[:,i], label=f"label{i}")
                # ax.legend()
                # plt.title(target)
                # plt.show()
                # # 创建包含15个子图的图形，每个子图对应一个通道
                # fig, axes = plt.subplots(15, 1, figsize=(8, 10), sharex=True)
                # # 定义一组不同的颜色，可以根据需要增加更多颜色
                # colors = ['red', 'blue', 'green', 'purple', 'orange', 'brown', 'pink', 'gray', 'cyan', 'magenta',
                #         'yellow', 'lime', 'teal', 'navy', 'black','red','red' ,'red']

                # # 遍历每个通道，并在对应的子图上绘制数据
                # for channel_idx in range(15):
                #     axes[channel_idx].plot((batch[0].cpu().detach().numpy())[:,channel_idx], color=colors[channel_idx])
                #     # axes[channel_idx].set_yticks([])  # 去除 Y 轴刻度和标签
                #     # axes[channel_idx].axis('off')  # 去除子图的边框和背景
                # # 设置整体图的标题
                # plt.suptitle("Data for Each Channel", fontsize=14)

                # # 调整子图之间的间距，使得每个通道之间不重叠
                # plt.subplots_adjust(hspace=0)
                # # 展示图形
                # plt.show()
                # test_loss += loss.item() / len(test_dataloader)
                test_preds += pred.detach().cpu().numpy().tolist()
                test_targets += target.detach().cpu().numpy().tolist()

        f1 = f1_score(test_targets, test_preds, average="macro")
        _, test_precision, test_recall, _ = precision_recall_fscore_support(
            test_targets, test_preds, labels=np.arange(2)
        )
        test_accuracy = accuracy_score(test_targets, test_preds)
        cm = confusion_matrix(test_targets, test_preds)
        # losslist.append(test_loss)
        f1s.append(f1)
        precision.append(test_precision)
        recall.append(test_recall)
        accuracy.append(test_accuracy)
        cmlist.append(cm)
        print(f"F1 Score = {f1}")
        print(f"Val Precision = {test_precision}, Recall = {test_recall}")
        print_colorful_text(f"Accuracy:{test_accuracy}", 'yellow')
        print("Confusion Matrix:")
        print(cm)
        if (epoch %100 == 0 and epoch != 0) or np.all(test_accuracy > 0.85) or test_accuracy>0.86:
            model.save(f'models/model_first_spike_{epoch}_{test_accuracy}.pth')
            print('模型已保存')
            np.save('train_data_record/loss.npy', losslist)
            np.save('train_data_record/f1s.npy', f1s)
            np.save('train_data_record/precision.npy', precision)
            np.save('train_data_record/recall.npy', recall)
            np.save('train_data_record/accuracy.npy', accuracy)
            np.save('train_data_record/cm.npy', cmlist)
            print('训练已完成，训练参数已保存') 
            


def snn_train_first_spike(device, train_dataloader, test_dataloader, model):
    criterion = torch.nn.CrossEntropyLoss()
    criterion.to(device)
    model.to(device)
    print('device:',device)
    opt = torch.optim.Adam(model.parameters().astorch(), lr=lr)
    losslist = []
    accuracy = []
    f1s = []
    precision = []
    recall = []
    cmlist= []
    train_bar = tqdm(range(num_epochs), desc=f"Loss = 0.0000")
    for epoch in train_bar:
        # scheduler.step()
        test_preds = []
        test_targets = []
        sum_loss = 0.0
        no_spike_num = 0
        for batch, target in (train_dataloader):
            batch = batch.to(torch.float32).to(device)
            target = target.to(device)
            model.reset_state()
            opt.zero_grad()
            out_model, _, rec = model(batch, record=True)
            # out = torch.sum(out_model,dim=1)
            loss = CACULATE_LOSS(data=(rec['4_LIFExodus']['vmem'].transpose(1, 2)),
                                 label=target,
                                 thr = thr_out)
            loss.backward()
            opt.step()
            # out, no_spike_num = detect_spike_analog(rec,thr_out+2.0,no_spike_num)
        for batch, target in (test_dataloader):
            with torch.no_grad():
                batch = batch.to(torch.float32).to(device)
                target = target.to(device)
                model.reset_state()
                out_model, _, rec = model(batch, record=True)
                out, no_spike_num = detect_spike_analog(rec,thr_out+2.0,no_spike_num)
                pred = out.argmax(1)
                test_preds += pred.tolist()
                test_targets += target.tolist()
                sum_loss += loss.item() / len(train_dataloader)
            train_bar.set_description(f"Loss = {loss:.4f}")
            
        print(f'共有{no_spike_num}个sample没有产生脉冲')
        cm = confusion_matrix(test_targets, test_preds)
        test_accuracy = accuracy_score(test_targets, test_preds)

        # print(f"Test Epoch = {epoch+1}, Loss = {sum_loss}, F1 Score = {sum_f1}")
        # print(f"Test Precision = {test_precision}, Recall = {test_recall}")
        print_colorful_text(f"Test Accuracy = {test_accuracy}",color='yellow')
        print(cm)
        sum_f1 = f1_score(test_targets, test_preds, average="macro")
        _, test_precision, test_recall, _ = precision_recall_fscore_support(
            test_targets, test_preds, labels=np.arange(5)
        )
        f1s.append(sum_f1)
        precision.append(test_precision)
        recall.append(test_recall)
        accuracy.append(test_accuracy)
        cmlist.append(cm)
        if  test_accuracy > 0.973:
            model.save(f'/home/liruixin/workspace/imu_classification/models/model_fspike_{time_step}_{test_accuracy}.pth')
            print('模型已保存')
            np.save('test_data_record/loss.npy', losslist)
            np.save('test_data_record/f1s.npy', f1s)
            np.save('test_data_record/precision.npy', precision)
            np.save('test_data_record/recall.npy', recall)
            np.save('test_data_record/accuracy.npy', accuracy)
            np.save('test_data_record/cm.npy', cmlist)
            print('训练已完成，训练参数已保存') 
            break